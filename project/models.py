from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django_countries.fields import CountryField

from project.choices import PROJECT_GRAPHIC_TWO_CHOICES
from project.validators import validate_file_extension
from user.models import User, ScientistRank, ScientistLevel, Appeal


class Type(models.Model):
    name = models.CharField(verbose_name='Название', max_length=10000)
    content = RichTextUploadingField(verbose_name='Контент')
    percent = models.FloatField(default=0)
    percent_title = models.CharField(verbose_name='Название процентуры', max_length=10000)
    amount = models.IntegerField(default=0, verbose_name='Сумма')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Типы'
        verbose_name_plural = 'Типы'


class TypeCategory(models.Model):
    type = models.ForeignKey(Type, verbose_name='Тип', on_delete=models.SET_NULL, null=True)
    name = models.CharField(verbose_name='Имя', max_length=10000)
    amount = models.IntegerField(verbose_name='Сумма')
    percent = models.FloatField(default=0)
    percent_title = models.CharField(verbose_name='Название процентуры', max_length=10000)

    def __str__(self):
        return f"{self.name}|{self.amount}"

    class Meta:
        verbose_name = 'Категория проекта'
        verbose_name_plural = 'Категории проектов'


class TypeEvent(models.Model):
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    start_time = models.DateTimeField(verbose_name='Начало')
    end_time = models.DateTimeField(verbose_name='Конец')
    type = models.ForeignKey(Type, verbose_name='Тип', on_delete=models.CASCADE)
    content = RichTextUploadingField(verbose_name='Контент')

    def __str__(self):
        return f"{self.type} | {self.start_time} | {self.end_time}"

    class Meta:
        verbose_name = 'Типы событий'
        verbose_name_plural = 'Типы событий'


class ProjectSphere(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=10000)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Сферы проектов'
        verbose_name_plural = 'Сферы проектов'


class ProjectOrder(models.Model):
    type = models.ForeignKey(Type, verbose_name='Тип', on_delete=models.SET_NULL, null=True)
    type_event = models.ForeignKey(TypeEvent, verbose_name='Тип события', on_delete=models.SET_NULL, null=True)
    type_category = models.ForeignKey(TypeCategory, verbose_name='Категория типов', on_delete=models.SET_NULL,
                                      null=True)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True)
    project_sphere = models.ForeignKey(ProjectSphere, verbose_name='Сфера проекта', on_delete=models.SET_NULL,
                                       null=True)
    title = models.CharField(verbose_name='Название', max_length=10000)
    status = models.SmallIntegerField(verbose_name='Статус', default=0)
    status_level_one = models.SmallIntegerField(verbose_name='Первый уровень статуса', default=0)
    status_level_two = models.SmallIntegerField(verbose_name='Второй уровень статуса', default=0)
    percent = models.FloatField(verbose_name='процент', default=0)

    stage_1_percent = models.FloatField(verbose_name='Проценты 1 шага', default=0)
    stage_2_percent = models.FloatField(verbose_name='Проценты 2 шага', default=0)
    stage_3_percent = models.FloatField(verbose_name='Проценты 3 шага', default=0)
    stage_4_percent = models.FloatField(verbose_name='Проценты 4 шага', default=0)
    stage_5_percent = models.FloatField(verbose_name='Проценты 5 шага', default=0)

    stage_1_fields = ArrayField(base_field=models.CharField(max_length=10000), null=True, blank=True)
    stage_2_fields = ArrayField(base_field=models.CharField(max_length=10000), null=True, blank=True)
    stage_3_fields = ArrayField(base_field=models.CharField(max_length=10000), null=True, blank=True)
    stage_4_fields = ArrayField(base_field=models.CharField(max_length=10000), null=True, blank=True)
    stage_5_fields = ArrayField(base_field=models.CharField(max_length=10000), null=True, blank=True)

    ready_for_send = models.BooleanField(default=False)
    sent_first_step_date = models.DateTimeField(auto_now_add=False, null=True, blank=True)
    sent_second_step_date = models.DateTimeField(auto_now_add=False, null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Заказы проектов'
        verbose_name_plural = 'Заказы проектов'


class ProjectOrderGraphicOne(models.Model):
    project_order = models.ForeignKey(ProjectOrder, verbose_name='Заказ', on_delete=models.CASCADE, null=True,
                                      related_name='graphic_one')
    content = RichTextUploadingField(verbose_name='Контент', null=True, blank=True, default="")
    start_time = models.DateField(verbose_name='Начало', null=True, blank=True)
    end_time = models.DateField(verbose_name='Конец', null=True, blank=True)
    info = models.TextField(verbose_name='Информация', null=True, blank=True, default="")
    interregional_nature = RichTextUploadingField(verbose_name='Межрегиональный характер работы', max_length=10000, default="", null=True, blank=True)
    work_quality = models.BooleanField(verbose_name='Качество работы (наличие рецензии/рекомендации известного ученого)', default=False)
    work_quality_file = models.FileField(verbose_name='Рецензии/Рекомендации известного ученого', null=True, blank=True)
    work_readiness_a = models.BooleanField(verbose_name='Отсутствие необходимости проведения дополнительных исследований/ доработок', default=False)
    work_readiness_b = models.BooleanField(verbose_name='Проведена редактура', default=False)
    work_readiness_c = models.BooleanField(verbose_name='Произведена верстка', default=False)
    novelty_a = models.BooleanField(verbose_name='Отсутствие значительных частей работы, опубликованных ранее', default=False)
    novelty_b = models.BooleanField(verbose_name='Наличие результатов исследований, сделанных в течение последних пяти лет', default=False)
    novelty_c = RichTextUploadingField(verbose_name='Научный задел (наличие ранее проведенных исследований, требующих завершения в рамках поданного проекта)', max_length=10000, default="", null=True, blank=True)
    novelty_d = models.BooleanField(verbose_name='Новизна (подтверждение новизны исследований не менее чем двумя экспертами в данной области знаний)', default=False)
    novelty_e = models.BooleanField(verbose_name='Подготовка к публикации в течение последующего после проведенного исследования года (а. научная статья (обозначение четкого планирования, стоимости и источника финансирования публикации)', default=False)
    novelty_f = models.BooleanField(verbose_name='Подготовка к публикации в течение последующего после проведенного исследования года (б. монография (обозначение четкого планирования, стоимости и источника финансирования публикации))', default=False)
    novelty_g = models.BooleanField(verbose_name='Подготовка к публикации в течение последующего после проведенного исследования года (материалы конференций, семинаров и т.д. - обозначение четкого планирования, стоимости и источника финансирования публикации)', default=False)
    have_a_good_translate = models.BooleanField(verbose_name='Наличие качественного перевода', default=False)
    have_a_good_translate_file = models.FileField(verbose_name='Файл перевода', null=True, blank=True)
    novelty_b_file = models.FileField(verbose_name='Файл наличия результатов исследований', null=True, blank=True)
    novelty_d_file = models.FileField(verbose_name='Файл наличия результатов исследований', null=True, blank=True)
    relevance = RichTextUploadingField(verbose_name='Актуальность и востребованность', max_length=10000, default="", blank=True, null=True)
    additionaly_amount = models.BooleanField(verbose_name='Дополнительное финансирование', default=False)
    additionaly_amount_file = models.FileField(verbose_name='Документы доплнительного финансирования', null=True, blank=True)

    def __str__(self):
        return f"{self.id}"

    class Meta:
        verbose_name = 'Первые графики'
        verbose_name_plural = 'Первые графики'


class ProjectOrderMonograph(models.Model):
    plan = models.TextField(verbose_name='Планирование', null=True, blank=True, default=0)
    cost = models.IntegerField(verbose_name='Стоимост', null=True, blank=True, default='')
    funding = models.TextField(verbose_name='Источник финансирование')
    graphic_one = models.ForeignKey('ProjectOrder', on_delete=models.CASCADE)

    def __str__(self):
        return self.plan

    class Meta:
        ordering = ['id']


class ProjectOrderResearchArticle(models.Model):
    plan = models.TextField(verbose_name='Планирование', null=True, blank=True, default='')
    cost = models.IntegerField(verbose_name='Стоимост', null=True, blank=True, default=0)
    funding = models.TextField(verbose_name='Источник финансирование')
    graphic_one = models.ForeignKey('ProjectOrder', on_delete=models.CASCADE)

    def __str__(self):
        return self.plan

    class Meta:
        ordering = ['id']


class ProjectOrderGraphicTwo(models.Model):
    project_order = models.ForeignKey(ProjectOrder, verbose_name='Заказ', on_delete=models.CASCADE, null=True,
                                      blank=True, related_name='graphic_two')
    first_name = models.CharField(verbose_name='Имя', max_length=10000, null=True, blank=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=10000, null=True, blank=True)
    middle_name = models.CharField(verbose_name='middle_name', max_length=10000, null=True, blank=True)
    appeal = models.ForeignKey(Appeal, verbose_name='Обращение', on_delete=models.CASCADE, null=True)
    birthday = models.DateField(verbose_name='День рождения', null=True)
    scientist_level = models.ForeignKey(ScientistLevel, verbose_name='Научная степень', on_delete=models.CASCADE,
                                        null=True, blank=True)
    scientist_level_year = models.SmallIntegerField(verbose_name='Год', null=True, blank=True)
    scientist_rank = models.ForeignKey(ScientistRank, verbose_name='Научное звание', on_delete=models.CASCADE,
                                       null=True, blank=True)
    scientist_rank_year = models.SmallIntegerField(verbose_name='Год', default=0, null=True, blank=True)
    work_place = models.TextField(verbose_name='Место работы', blank=True)
    work_place_short = models.CharField(verbose_name='Коротко о месте работы', max_length=10000, blank=True)
    work_position = models.CharField(verbose_name="Позиция на работе", max_length=10000, blank=True)
    public_count = models.IntegerField(verbose_name='Количество публики', default=0, blank=True)
    address_zip = models.CharField(verbose_name='Zip адресс', max_length=10000)
    address_country = CountryField(verbose_name='Страна', blank=True, max_length=10000)
    address_region = models.CharField(verbose_name='Регион', max_length=10000)
    address_street = models.CharField(verbose_name='Улица', max_length=10000)
    work_phone_number = models.CharField(verbose_name='Рабочий телефон', max_length=10000)
    home_phone_number = models.CharField(verbose_name='Домашний телефон', max_length=10000)
    email = models.EmailField(verbose_name='Адресс электронной почты')
    project_position = models.CharField(max_length=10000, choices=PROJECT_GRAPHIC_TWO_CHOICES,
                                        null=True, blank=True, default='executor')
    additionaly_data = models.TextField(verbose_name='Дополнительные данные', null=True, blank=True)

    def __str__(self):
        return f"{self.first_name}|{self.last_name}"

    class Meta:
        verbose_name = 'Вторые графики'
        verbose_name_plural = 'Вторые графики'


class ProjectOrderGraphicThree(models.Model):
    project_order = models.ForeignKey(ProjectOrder, verbose_name='Заказ', on_delete=models.CASCADE, null=True,
                                      blank=True, related_name='graphic_three')

    def __str__(self):
        return str(self.project_order)

    class Meta:
        verbose_name = 'Третьи грфики'
        verbose_name_plural = 'Третьи грфики'


class ProjectOrderGraphicFour(models.Model):
    project_order = models.ForeignKey(ProjectOrder, verbose_name='Заказ', on_delete=models.CASCADE, null=True,
                                      related_name='graphic_four')
    name = models.CharField(verbose_name='Название', max_length=10000)
    count = models.FloatField(verbose_name='Количество', default=0)
    unit_type = models.CharField(verbose_name='Тип', max_length=10000, default='', null=True, blank=True)
    price = models.IntegerField(verbose_name='Цена', default=0)
    amount = models.IntegerField(verbose_name='Сумма', default=0)
    extra_amount = models.IntegerField(verbose_name='Сумма гранта', default=0)
    extra_text = models.CharField(verbose_name='Текст гранта', max_length=10000, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Четвертые грфики'
        verbose_name_plural = 'Четвертые грфики'
        ordering = ['id']


class ProjectOrderGraphicFile(models.Model):
    project_order = models.ForeignKey(ProjectOrder, verbose_name='Заказ', on_delete=models.CASCADE, null=True,
                                      related_name='graphic_files')
    file_ru = models.FileField(verbose_name='Файл(Ру)', validators=[validate_file_extension], null=True, blank=True)
    file_en = models.FileField(verbose_name='Файл(En)', validators=[validate_file_extension], null=True, blank=True)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)

    def __str__(self):
        return f"{self.file_ru}"

    class Meta:
        verbose_name = 'Файлы графиков'
        verbose_name_plural = 'Файлы графиков'


class ProjectOrderGraphicThreeResult(models.Model):
    project_order_graphic_three = models.ForeignKey(ProjectOrderGraphicThree, verbose_name='График',
                                                    on_delete=models.CASCADE, null=True,
                                                    related_name='graphic_three_result')
    main_step = models.TextField(verbose_name='Этап выполнения проекта', blank=True, null=True, default="")
    attr_one = models.CharField(verbose_name='Сроки реализации ', max_length=10000)
    attr_two = models.CharField(verbose_name='Выполняемая работа на Русском', max_length=10000)
    attr_three = models.CharField(verbose_name='Контрольные результаты на Русском', max_length=10000)

    def __str__(self):
        return self.attr_one

    class Meta:
        verbose_name = 'Результаты третьих графиков'
        verbose_name_plural = 'Результаты третьих графиков'


class ProjectOrderMark(models.Model):
    content = models.TextField(null=True, blank=True, default="")
    project_order = models.ForeignKey(ProjectOrder, verbose_name='Заказ', on_delete=models.CASCADE, null=True,
                                      related_name='order_mark')
    created = models.DateTimeField(verbose_name="Создано", auto_now_add=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True)
    mark = models.SmallIntegerField(verbose_name='Оценка', default=0)
    project_mark = models.ForeignKey('project.ProjectMark',
                                     on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.mark} | {self.user}"

    class Meta:
        verbose_name = 'Отметки заказов'
        verbose_name_plural = 'Отметки заказов'


class ProjectMark(models.Model):
    type = models.ForeignKey(Type, verbose_name='Тип', on_delete=models.SET_NULL, null=True)
    name = models.CharField(verbose_name='Название', max_length=10000)
    min_mark = models.IntegerField(verbose_name='Минимальная ставка', default=0)
    max_mark = models.IntegerField(verbose_name='Максимальная ставка', default=0)
    prior = models.IntegerField(verbose_name='Приоритет', default=0)
    content = RichTextUploadingField(verbose_name='Контент', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Отметки'
        verbose_name_plural = 'Отметки'
        ordering = ['prior']


class ProjectOrderComment(models.Model):
    project_order = models.ForeignKey(ProjectOrder, verbose_name='Заказ', on_delete=models.CASCADE, null=True,
                                      related_name='order_comment')
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True)
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='receiver')
    content = RichTextUploadingField(verbose_name='Контент')

    def __str__(self):
        return f"{self.user}|{self.project_order}"

    class Meta:
        verbose_name = 'Комментарии'
        verbose_name_plural = 'Коментарии'


class ProjectOrderAlert(models.Model):
    project_order = models.ForeignKey(ProjectOrder, verbose_name='Заказ', on_delete=models.CASCADE, null=True,
                                      related_name='order_alert')
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    title = models.CharField(verbose_name='Название', max_length=10000)
    content = RichTextUploadingField(verbose_name='Контент')
    seen = models.IntegerField(verbose_name='seen', default=0)
    user = models.ForeignKey('user.User',
                             on_delete=models.CASCADE,
                             null=True,
                             blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тревога'
        verbose_name_plural = 'Тревога'


class Page(models.Model):
    prior = models.SmallIntegerField(default=0, verbose_name='Приоритет')
    title = models.CharField(verbose_name='Название', max_length=10000)
    content = RichTextUploadingField(verbose_name='Контент')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Страницы'
        verbose_name_plural = 'Страницы'
