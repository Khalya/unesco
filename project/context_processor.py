from django.utils.translation import activate

from project.models import Page


def pages(request):
    if request.user.is_authenticated:
        activate(request.user.lang)
    pages = Page.objects.order_by('prior')
    return {
        'pages': pages,
    }
