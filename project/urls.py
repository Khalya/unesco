from django.urls import path
from project.views import MainView, PageView, OrderView, OrderDetailView, MyFormView, MarksView, OrderLevelOneList, \
    OrderLevelTwo, OrderEventsResult, OrderEventsResultView, OrderLevelOne, OrderLevelTwoList, SetOrderStatus, \
    UserScores

urlpatterns = [
    path('', MainView.as_view(), name='main-page'),
    path('page/<int:pk>', PageView.as_view(), name='pages'),
    path('orders/', OrderView.as_view(), name='orders'),
    path('user/marks/', UserScores.as_view(), name='user_scores'),
    path('orders/status/', SetOrderStatus.as_view(), name='set_order_status'),
    path('orders/action/', MarksView.as_view(), name='orders_action'),
    path('orders/<int:pk>', OrderDetailView.as_view(), name='order-detail'),
    path('orders/my-orders/', MyFormView.as_view(), name='my-form'),
    path('orders/level/one/list/', OrderLevelOneList.as_view(), name='order-level-one-list'),
    path('orders/level/one/<int:pk>/', OrderLevelOne.as_view(), name='order-level-one'),

    path('orders/level/two/list/', OrderLevelTwoList.as_view(), name='order-level-two-list'),
    path('orders/level/two/<int:pk>/', OrderLevelTwo.as_view(), name='order-level-two'),

    path('orders/events/result/', OrderEventsResult.as_view(), name='order-events-result'),
    path('orders/events/result/view/<int:pk>', OrderEventsResultView.as_view(), name='order-events-result-view'),
]
