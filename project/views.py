from datetime import datetime

from django.db.models.functions import Coalesce
from django.http import JsonResponse
from django.db.models import Sum, Count, OuterRef, Subquery, Q
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import TemplateView, ListView, DetailView
from pytz import UTC
from sql_util.aggregates import SubqueryAggregate

from project.check_order_fill import check_all_stages
from project.models import TypeEvent, Page, ProjectOrder, ProjectOrderGraphicThree, ProjectOrderGraphicThreeResult, \
    ProjectOrderGraphicTwo, ProjectOrderGraphicOne, ProjectOrderGraphicFour, ProjectOrderComment, ProjectOrderAlert, \
    ProjectOrderMark, ProjectMark, Type, TypeCategory, ProjectOrderGraphicFile, ProjectOrderMonograph, \
    ProjectOrderResearchArticle
from user.models import User


class MainView(TemplateView):
    template_name = 'main_page.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MainView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data()
        type_event = TypeEvent.objects.order_by('-id').first()
        user_sent_project = ProjectOrder.objects.filter(user=self.request.user).count()
        context['event'] = type_event
        context['user_sent_project'] = user_sent_project > 0
        # context['user_sent_project'] = False
        return context


class PageView(DetailView):
    model = Page
    template_name = 'page.html'
    context_object_name = 'page'


class OrderView(ListView):
    template_name = 'orders.html'
    model = ProjectOrder
    context_object_name = 'projects'


class OrderDetailView(TemplateView):
    template_name = 'orders_details.html'

    def dispatch(self, request, *args, **kwargs):
        check_all_stages(self.kwargs['pk'])
        return super(OrderDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(OrderDetailView, self).get_context_data(**kwargs)
        order = ProjectOrder.objects.select_related('type', 'type_category', 'type_event').get(id=self.kwargs['pk'])
        context['project'] = order
        context['files_ready'] = True
        try:
            graphic_two = ProjectOrderGraphicTwo.objects.filter(project_order_id=self.kwargs['pk'])
            graphic_one = ProjectOrderGraphicOne.objects.get(project_order_id=order.id)
            context['graphic_one'] = graphic_one
            context['graphics_two'] = graphic_two
            context['project_monographs'] = ProjectOrderMonograph.objects.filter(graphic_one=order)
            context['project_research_articles'] = ProjectOrderResearchArticle.objects.filter(graphic_one=order)
            graphic_three = ProjectOrderGraphicThree.objects.get(project_order_id=order.id)
            context['graphic_three'] = graphic_three
            context['three_result'] = ProjectOrderGraphicThreeResult.objects.filter(
                project_order_graphic_three_id=graphic_three.id)
            graphic_four = ProjectOrderGraphicFour.objects.filter(project_order=order)
            amount = graphic_four.aggregate(Sum('amount'))['amount__sum']
            if not amount:
                amount = 0
            context['graphic_four'] = graphic_four
            context['amount'] = amount
            if order.sent_first_step_date:
                if order.sent_first_step_date.replace(tzinfo=UTC) > datetime(2021, 7, 17, 0, 0).replace(tzinfo=UTC):
                    iicas_percent = round(amount / 100 * order.type.percent)
                    context['graphic_four_sum'] = amount + iicas_percent
                    context['total'] = amount + iicas_percent
                    context['iicas_percent'] = iicas_percent
                else:
                    context['graphic_four_sum'] = amount
                    context['total'] = amount
                    context['iicas_percent'] = 0
            else:
                iicas_percent = round(amount / 100 * order.type.percent)
                context['graphic_four_sum'] = amount + iicas_percent
                context['total'] = amount + iicas_percent
                context['iicas_percent'] = iicas_percent
            context['alerts'] = ProjectOrderAlert.objects.filter(project_order_id=order.id).order_by('-id')
            comments = ProjectOrderComment.objects.filter(project_order_id=order.id).order_by('-id')
            if self.request.user.user_type == 'council' or self.request.user.user_type == 'user':
                comments = comments.filter(Q(user=self.request.user) | Q(receiver=self.request.user))
            context['comments'] = comments
            context['alerts_count'] = ProjectOrderAlert.objects.filter(project_order_id=order.id).count()
            context['comments_count'] = ProjectOrderComment.objects.filter(project_order_id=order.id).count()
            can_mark = ProjectOrderMark.objects.filter(project_order_id=self.kwargs['pk'], user=self.request.user)
            context['marks'] = ProjectMark.objects.filter(type=order.type).order_by('-prior')
            context['can_mark'] = can_mark.count() <= 0
            context['can_mark_content'] = can_mark.filter(project_mark__id__in=[12, 19, 29], user=self.request.user).first()
            context['files'] = ProjectOrderGraphicFile.objects.filter(project_order_id=self.kwargs['pk'])
            context['councils'] = User.objects.filter(user_type='council')
            interregional_project = graphic_two.distinct('address_country')
            context['interregional_regions'] = interregional_project
            context['interregional_project'] = interregional_project.count()
            if graphic_one.have_a_good_translate:
                if not graphic_one.have_a_good_translate_file_ru or not graphic_one.have_a_good_translate_file_en:
                    context['files_ready'] = False
            if graphic_one.novelty_d:
                if not graphic_one.novelty_d_file_ru or not graphic_one.novelty_d_file_en:
                    context['files_ready'] = False
            if graphic_one.additionaly_amount:
                if not graphic_one.additionaly_amount_file_ru or not graphic_one.additionaly_amount_file_en:
                    context['files_ready'] = False
        except ProjectOrderGraphicOne.DoesNotExist:
            print("EXCEPTION!")
            context['graphic_one'] = ''
        return context

    def post(self, request, pk):
        try:
            to_delete = request.POST.get('to_delete', None)
            action = request.POST.get('action', None)
            if to_delete == 'yes':
                ProjectOrderGraphicOne.objects.get(project_order_id=self.kwargs['pk']).delete()
                ProjectOrderGraphicTwo.objects.filter(project_order_id=self.kwargs['pk']).delete()
                ProjectOrderGraphicThree.objects.filter(project_order_id=self.kwargs['pk']).delete()
                ProjectOrderGraphicThreeResult.objects.filter(
                    project_order_graphic_three__project_order_id=self.kwargs['pk']).delete()
                ProjectOrderGraphicFour.objects.filter(project_order_id=self.kwargs['pk']).delete()
                ProjectOrder.objects.get(id=self.kwargs['pk']).delete()
                return redirect('/')
            if action == 'add_alert':
                title = request.POST.get('title', None)
                content = request.POST.get('content', None)
                user = request.POST.get('user', None)
                alert = ProjectOrderAlert.objects.create(project_order_id=pk,
                                                         title=title,
                                                         content=content,
                                                         user_id=user)
                alert_serialize = ProjectOrderAlert.objects.values().get(id=alert.id)
                _user = User.objects.get(pk=alert_serialize["user_id"])
                return JsonResponse({
                    'content': alert_serialize['content'],
                    'title': alert_serialize['title'],
                    'created': alert_serialize['created'].strftime('%d-%m-%Y | %H:%M'),
                    'user': f'{_user.first_name} | {_user.last_name}'
                }, safe=False)
            elif action == 'add_comment':
                try:
                    receiver = request.POST.get('receiver', None)
                    content = request.POST.get('content', None)
                    if receiver == 'applicant':
                        project_order = ProjectOrder.objects.get(pk=self.kwargs['pk'])
                        comment = ProjectOrderComment.objects.create(project_order_id=self.kwargs['pk'],
                                                                     content=content, user_id=request.user.id,
                                                                     receiver_id=project_order.user.id)
                    else:
                        comment = ProjectOrderComment.objects.create(project_order_id=self.kwargs['pk'],
                                                                     content=content, user_id=request.user.id,
                                                                     receiver_id=receiver)
                    comment_serialize = ProjectOrderComment.objects.values().get(id=comment.id)
                    _user = User.objects.get(pk=comment_serialize["user_id"])
                    if receiver:
                        receiver = f'{comment.receiver.last_name} {comment.receiver.first_name}'
                    else:
                        receiver = None
                    print(receiver)
                    return JsonResponse({
                        'content': comment_serialize['content'],
                        'created': comment_serialize['created'].strftime('%d-%m-%Y | %H:%M'),
                        'user': f'{_user.first_name} | {_user.last_name}',
                        'receiver': receiver
                    }, safe=False)
                except Exception as e:
                    print(e)
                    return JsonResponse({
                        'comment': e
                    }, safe=False)
            elif action == 'add_mark':
                print(request.POST)
                p_order = ProjectOrder.objects.select_related('type').get(pk=pk)
                marks = ProjectMark.objects.filter(type_id=p_order.type.id).exclude(id__in=[12, 29, 19])

                bulks = []
                for mark in marks:
                    bulks.append(ProjectOrderMark(
                        project_order_id=pk,
                        project_mark=mark,
                        user=request.user,
                        mark=request.POST.get(f'mark_{mark.id}', 0)
                    ))
                if p_order.type.id == 1:
                    bulks.append(ProjectOrderMark(
                        project_order_id=pk,
                        project_mark_id=12,
                        user=request.user,
                        content=request.POST.get('content_1', ''),
                        mark=request.POST.get(f'mark_12', 0)
                    ))
                elif p_order.type.id == 2:
                    bulks.append(ProjectOrderMark(
                        project_order_id=pk,
                        project_mark_id=29,
                        user=request.user,
                        content=request.POST.get('content_2', ''),
                        mark=request.POST.get(f'mark_29', 0)
                    ))
                elif p_order.type.id == 3:
                    bulks.append(ProjectOrderMark(
                        project_order_id=pk,
                        project_mark_id=19,
                        user=request.user,
                        content=request.POST.get('content_3', ''),
                        mark=request.POST.get(f'mark_19', 0)
                    ))
                ProjectOrderMark.objects.bulk_create(bulks)

                # for mark in marks_value:
                #     order_mark.project_order_mark.add(mark)
                # order_mark.pro

            return redirect(f"/orders/{pk}")
        except Exception as e:
            return JsonResponse({
                'message_error': str(e)
            }, safe=False)


class MyFormView(ListView):
    template_name = 'my_orders.html'
    model = ProjectOrder
    context_object_name = 'project_orders'

    def get_queryset(self):
        queryset = ProjectOrder.objects.prefetch_related('graphic_one').filter(user_id=self.request.user.id)
        return queryset

    def post(self, request):
        action = request.POST.get('action')
        if action == 'delete':
            delete_pk = request.POST.get('delete_pk')
            ProjectOrder.objects.get(pk=delete_pk).delete()
        return redirect(reverse_lazy('my-form'))


class MarksView(View):
    def get(self, request):
        pk = request.GET.get('pk', None)
        if not pk:
            return JsonResponse(
                {
                    'error': True
                },
                safe=False
            )
        project_order = ProjectOrder.objects.get(pk=pk)
        print(request.user.user_type)
        if request.user.user_type == 'council':
            users = User.objects.filter(pk=request.user.id) \
                .distinct().values('first_name', 'id', 'last_name')
            user_marks = ProjectOrderMark.objects.filter(project_order_id=pk,
                                                         project_mark__type=project_order.type,
                                                         user_id=request.user.id).in_bulk()
        else:
            users = User.objects.filter(projectordermark__project_order_id=pk) \
                .distinct().values('first_name', 'id', 'last_name')
            user_marks = ProjectOrderMark.objects.filter(project_order_id=pk,
                                                         project_mark__type=project_order.type).in_bulk()
        result_dict = dict()
        marks = ProjectMark.objects.filter(type=project_order.type).order_by('prior') \
            .values('id', 'name', 'content', 'name_en', 'content_en')

        for user in users.values('first_name', 'id', 'last_name'):
            user_dict = dict(user)
            result_dict[user_dict.get('id')] = user_dict
            result_dict[user_dict.get('id')]['project_marks'] = dict()
            for user_mark in user_marks.values():
                user_mark_dict = dict(user_mark.__dict__)
                if '_state' in user_mark_dict:
                    user_mark_dict.__delitem__('_state')
                if user_mark_dict.get('user_id') == user.get('id'):
                    result_dict[user_dict.get('id')]['project_marks'][
                        user_mark_dict.get('project_mark_id')] = user_mark_dict
        return JsonResponse(
            {
                'users': result_dict,
                'marks': list(marks)
            },
            safe=False
        )


class OrderLevelOneList(ListView):
    template_name = 'order_level_one_list.html'
    model = TypeEvent
    paginate_By = 1000

    def get_queryset(self):
        p_orders = ProjectOrder.objects.filter(status_level_one=1, status=1, type_event_id=OuterRef('pk')) \
            .values('type_event_id') \
            .annotate(count=Count('pk')).order_by('-pk')

        return TypeEvent.objects.annotate(c=Subquery(p_orders.values('count')[:1])).order_by('-pk')


class OrderLevelOne(ListView):
    template_name = 'orders_level_1.html'
    model = ProjectOrder
    paginate_By = 1000

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(OrderLevelOne, self).get_context_data(**kwargs)
        start_time = self.request.GET.get('start_time', None)
        end_time = self.request.GET.get('end_time', None)
        context['type_event'] = TypeEvent.objects.get(pk=self.kwargs['pk'])
        if start_time and end_time:
            context['start_time'] = start_time
            context['end_time'] = end_time
        return context

    def get_queryset(self):
        start_time = self.request.GET.get('start_time', None)
        end_time = self.request.GET.get('end_time', None)
        if start_time and end_time:
            return ProjectOrder.objects. \
                select_related('type', 'type_event', 'user', 'project_sphere', 'type_category'). \
                filter(type_event_id=self.kwargs['pk'], status=1, status_level_one=1, created__range=[start_time, end_time]).order_by('-pk')
        else:
            return ProjectOrder.objects. \
                select_related('type', 'type_event', 'user', 'project_sphere', 'type_category'). \
                filter(type_event_id=self.kwargs['pk'], status=1, status_level_one=1).order_by('-pk')


class OrderLevelTwoList(ListView):
    template_name = 'order_level_two_list.html'
    model = TypeEvent
    paginate_By = 1000

    def get_queryset(self):
        p_orders = ProjectOrder.objects \
            .filter(status_level_one=1, status=1, status_level_two=1, type_event_id=OuterRef('pk')) \
            .values('type_event_id') \
            .annotate(count=Count('id'))

        return TypeEvent.objects.annotate(c=Subquery(p_orders.values('count')[:1])).order_by('-pk')


class OrderLevelTwo(ListView):
    template_name = 'orders_level_2.html'
    model = ProjectOrder
    paginate_By = 1000

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(OrderLevelTwo, self).get_context_data(**kwargs)
        context['type_event'] = TypeEvent.objects.get(pk=self.kwargs['pk'])
        return context

    def get_queryset(self):
        user_score = ProjectOrderMark.objects.filter(user=self.request.user, project_order_id=OuterRef('pk'))
        return ProjectOrder.objects. \
            select_related('type', 'type_event', 'user', 'project_sphere', 'type_category'). \
            filter(type_event_id=self.kwargs['pk'], status=1, status_level_one=1, status_level_two=1).\
            order_by('-pk').\
            annotate(user_score=Subquery(user_score.values('pk')[:1]))


class OrderEventsResult(ListView):
    template_name = 'order_events_result.html'
    model = TypeEvent
    paginate_By = 1000

    def get_queryset(self):
        p_orders = ProjectOrder.objects \
            .filter(status_level_one=1, status=1, status_level_two=1, type_event_id=OuterRef('pk')) \
            .values('type_event_id') \
            .annotate(count=Count('id')).order_by('-pk')
        return TypeEvent.objects.annotate(c=Subquery(p_orders.values('count')[:1])).order_by('-pk')


class OrderEventsResultView(ListView):
    template_name = 'order_events_result_view.html'
    model = ProjectOrder
    paginate_by = 15

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(OrderEventsResultView, self).get_context_data(**kwargs)
        context['type_event'] = TypeEvent.objects.get(pk=self.kwargs['pk'])
        context['types'] = Type.objects.all()
        context['type_categories'] = TypeCategory.objects.all()
        context['event_type'] = self.request.GET.get('event_type', 'all')
        context['event_type_category'] = self.request.GET.get('event_type_category', 'all')
        return context

    def get_queryset(self):
        print(self.kwargs)
        agg = SubqueryAggregate('order_mark__mark', aggregate=Sum, filter=Q(project_order_id=OuterRef('id')))
        queryset = ProjectOrder.objects. \
            select_related('type', 'type_event', 'user', 'project_sphere', 'type_category'). \
            filter(type_event_id=self.kwargs['pk'], status=1, status_level_one=1, status_level_two=1) \
            .annotate(c=agg)
        event_type_category = self.request.GET.get('event_type_category', None)
        event_type = self.request.GET.get('event_type', None)
        if event_type and event_type != 'all':
            queryset = queryset.filter(type_id=event_type)
        if event_type_category and event_type_category != 'all':
            queryset = queryset.filter(type_category_id=event_type_category)
        return queryset


class SetOrderStatus(View):
    def get(self, request):
        pk = request.GET.get('pk', 0)
        level = request.GET.get('level', 0)
        if int(level) == 0:
            ProjectOrder.objects.filter(pk=pk).update(status=1)
        elif int(level) == 1:
            ProjectOrder.objects.filter(pk=pk).update(status_level_one=1, sent_first_step_date=datetime.now())
        elif int(level) == 2:
            ProjectOrder.objects.filter(pk=pk).update(status_level_two=1, sent_second_step_date=datetime.now())
        elif int(level) == 11:
            ProjectOrder.objects.filter(pk=pk).update(status_level_two=0,
                                                      status_level_one=0,
                                                      status=0)

        return redirect(reverse('order-detail', kwargs={"pk": request.GET.get('pk')}))


class UserScores(ListView):
    model = ProjectOrderMark
    template_name = 'user_scores.html'

    def get_queryset(self):
        category = self.request.GET.get('category', '')
        query = ProjectOrder.objects.select_related('user').filter(sent_first_step_date__isnull=False).values('user') \
            .annotate(total_marks=Coalesce(Sum('order_mark__mark'), 0)) \
            .values('user__first_name', 'user__last_name', 'total_marks', 'title') \
            .order_by('-total_marks', 'sent_first_step_date')
        categories = TypeCategory.objects.select_related('type')
        context = {'query': query, 'categories': categories}
        if category:
            context['query'] = query.filter(type_category_id=category)
            context['selected_category'] = TypeCategory.objects.get(id=category)
            context['categories'] = categories.exclude(id=category)
            return context
        else:
            return context
