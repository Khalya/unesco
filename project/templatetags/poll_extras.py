from django import template

register = template.Library()


@register.filter(name='get_by_index')
def get_by_index(indexable, i):
    try:
        return indexable[i]
    except IndexError:
        return None


@register.filter(name='get_by_key')
def get_by_key(indexable, i):
    try:
        return indexable[str(i)]
    except:
        return None


@register.filter(name='get_name_by_leng')
def get_by_key(obj, lang):
    print(obj.custom_countries)
    return obj.name


@register.filter(name='multiply')
def multiply(a, b):
    return a*b


@register.filter(name='to_str')
def to_str(val):
    return str(val)


@register.filter(name='division')
def division(a, b):
    return round(a/b, 2)


@register.filter(name='plus')
def plus(a, b):
    return a+b


@register.filter(name='range')
def times(number_min, number_max):
    return range(number_min, number_max + 1)
