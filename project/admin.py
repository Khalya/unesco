from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TabbedTranslationAdmin

from project.models import Type, TypeEvent, ProjectOrder, ProjectMark, ProjectSphere, ProjectOrderAlert, \
    ProjectOrderMark, ProjectOrderGraphicTwo, ProjectOrderComment, ProjectOrderGraphicThreeResult, \
    ProjectOrderGraphicFile, ProjectOrderGraphicFour, ProjectOrderGraphicThree, ProjectOrderGraphicOne, Page, \
    TypeCategory, ProjectOrderMonograph, ProjectOrderResearchArticle


@admin.register(ProjectOrderMark)
class ProjectOrderMarkAdmin(admin.ModelAdmin):
    pass


@admin.register(ProjectOrderComment)
class ProjectOrderCommentAdmin(admin.ModelAdmin):
    pass


@admin.register(ProjectOrderGraphicFile)
class ProjectOrderGraphicFileAdmin(admin.ModelAdmin):
    pass


@admin.register(Type)
class TypeAdmin(TabbedTranslationAdmin):
    pass


@admin.register(TypeEvent)
class TypeEventAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ProjectOrder)
class ProjectOrderAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ProjectMark)
class ProjectMarkAdmin(TabbedTranslationAdmin):
    list_display = 'id', 'name', 'content', 'type', 'min_mark', 'max_mark'
    list_filter = 'type',


@admin.register(ProjectSphere)
class ProjectSphereAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ProjectOrderAlert)
class ProjectOrderAlertAdmin(admin.ModelAdmin):
    pass


@admin.register(ProjectOrderGraphicTwo)
class ProjectOrderGraphicTwoAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ProjectOrderGraphicFour)
class ProjectOrderGraphicFourAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ProjectOrderGraphicThree)
class ProjectOrderGraphicThreeAdmin(admin.ModelAdmin):
    pass


@admin.register(ProjectOrderGraphicThreeResult)
class ProjectOrderGraphicThreeResultAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ProjectOrderGraphicOne)
class ProjectOrderGraphicOneAdmin(TabbedTranslationAdmin):
    pass


@admin.register(Page)
class PageAdmin(TabbedTranslationAdmin):
    pass


@admin.register(TypeCategory)
class TypeCategoryAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ProjectOrderMonograph)
class ProjectOrderMonographAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ProjectOrderResearchArticle)
class ProjectOrderResearchArticleAdmin(TabbedTranslationAdmin):
    pass
