from modeltranslation.translator import register, TranslationOptions
from project.models import Type, TypeEvent, ProjectOrder, ProjectMark, ProjectSphere, \
    ProjectOrderGraphicTwo, ProjectOrderGraphicThreeResult, ProjectOrderGraphicFour, \
    ProjectOrderGraphicOne, Page, TypeCategory, ProjectOrderMonograph, \
    ProjectOrderResearchArticle


@register(Type)
class TypeTranslationOptions(TranslationOptions):
    fields = ('name', 'content', 'percent_title')


@register(TypeEvent)
class TypeEventTranslationOptions(TranslationOptions):
    fields = ('content',)


@register(ProjectOrder)
class ProjectOrderTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(ProjectMark)
class ProjectMarkTranslationOptions(TranslationOptions):
    fields = ('name', 'content')


@register(ProjectSphere)
class ProjectSphereTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(ProjectOrderGraphicTwo)
class ProjectOrderGraphicTwoTranslationOptions(TranslationOptions):
    fields = ('work_place', 'work_place_short', 'work_position', 'first_name', 'last_name', 'middle_name',
              'additionaly_data')


@register(ProjectOrderGraphicThreeResult)
class ProjectOrderGraphicThreeResultTranslationOptions(TranslationOptions):
    fields = ('attr_one', 'attr_two', 'attr_three', 'main_step')


@register(ProjectOrderGraphicFour)
class ProjectOrderGraphicFourTranslationOptions(TranslationOptions):
    fields = ('name', 'unit_type', 'extra_text')


@register(ProjectOrderGraphicOne)
class ProjectOrderGraphicOneTranslationOptions(TranslationOptions):
    fields = ('content', 'info', 'interregional_nature', 'relevance', 'work_quality_file',
              'have_a_good_translate_file', 'novelty_b_file', 'novelty_c', 'novelty_d_file',
              'additionaly_amount_file')


@register(Page)
class PageTranslationOptions(TranslationOptions):
    fields = ('title', 'content',)


@register(TypeCategory)
class PageTranslationOptions(TranslationOptions):
    fields = ('name', 'percent_title')


@register(ProjectOrderMonograph)
class PageTranslationOptions(TranslationOptions):
    fields = ('plan', 'funding')


@register(ProjectOrderResearchArticle)
class PageTranslationOptions(TranslationOptions):
    fields = ('plan', 'funding')
