from project.models import ProjectOrder, ProjectOrderGraphicOne, ProjectOrderGraphicTwo, ProjectOrderGraphicThree, \
    ProjectOrderGraphicThreeResult, ProjectOrderGraphicFour, ProjectOrderGraphicFile

SCORE_POINT = 5


def check_first_stage(project_order_id):
    try:
        project_order = ProjectOrder.objects.get(pk=project_order_id)
    except ProjectOrder.DoesNotExist:
        return False
    total_score = 0
    graphic, created = ProjectOrderGraphicOne.objects.get_or_create(project_order_id=project_order.id)
    fields = []
    graphic_two = ProjectOrderGraphicTwo.objects.filter(project_order_id=project_order.id).count()

    if graphic_two > 0:
        total_score += (SCORE_POINT * 2)
    else:
        fields.append({'stage': 1, 'field': 'performer'})
    if graphic.content_ru:
        total_score += SCORE_POINT
    else:
        fields.append({'stage': 1, 'field': 'content_ru'})
    if graphic.content_en:
        total_score += SCORE_POINT
    else:
        fields.append({'stage': 1, 'field': 'content_en'})
    if graphic.info_ru:
        total_score += SCORE_POINT
    else:
        fields.append({'stage': 1, 'field': 'info_ru'})
    if graphic.info_en:
        total_score += SCORE_POINT
    else:
        fields.append({'stage': 1, 'field': 'info_en'})
    if graphic.start_time:
        total_score += (SCORE_POINT * 2)
    else:
        fields.append({'stage': 1, 'field': 'start_time'})
    if graphic.end_time:
        total_score += (SCORE_POINT * 2)
    else:
        fields.append({'stage': 1, 'field': 'end_time'})
    if graphic.relevance_ru:
        total_score += SCORE_POINT
    else:
        fields.append({'stage': 1, 'field': 'end_time'})
    if graphic.relevance_en:
        total_score += SCORE_POINT
    else:
        fields.append({'stage': 1, 'field': 'end_time'})
    if graphic.interregional_nature_ru:
        total_score += SCORE_POINT
    else:
        fields.append({'stage': 1, 'field': 'end_time'})
    if graphic.interregional_nature_en:
        total_score += SCORE_POINT
    else:
        fields.append({'stage': 1, 'field': 'end_time'})
    project_order.stage_1_fields = fields
    project_order.stage_1_percent = total_score
    project_order.save()
    project_order.percent = calculate_project_order_percent(project_order)
    project_order.save()
    if fields:
        return {'state': False}
    else:
        return {'state': True}


def check_second_stage(project_order_id):
    try:
        project_order = ProjectOrder.objects.get(pk=project_order_id)
    except ProjectOrder.DoesNotExist:
        return False
    total_score = 0
    fields = []
    graphic, created = ProjectOrderGraphicThree.objects.get_or_create(project_order_id=project_order_id)
    graphic_result = ProjectOrderGraphicThreeResult.objects.\
        filter(project_order_graphic_three__project_order_id=project_order_id).count()

    if graphic_result > 0:
        total_score += (SCORE_POINT * 2)
    else:
        fields.append({'stage': 2, 'field': 'graphic_result'})
    project_order.stage_3_fields = fields
    project_order.stage_3_percent = total_score
    project_order.save()
    project_order.percent = calculate_project_order_percent(project_order)
    project_order.save()


def check_third_stage(project_order_id):
    try:
        project_order = ProjectOrder.objects.get(pk=project_order_id)
    except ProjectOrder.DoesNotExist:
        return False
    total_score = 0
    fields = []
    graphic_result = ProjectOrderGraphicFour.objects.\
        filter(project_order_id=project_order_id).count()
    if graphic_result > 0:
        total_score += (SCORE_POINT * 2)
    else:
        fields.append({'stage': 3, 'field': 'graphic_result'})
    project_order.stage_4_fields = fields
    project_order.stage_4_percent = total_score
    project_order.save()
    project_order.percent = calculate_project_order_percent(project_order)
    project_order.save()


def check_fourth_stage(project_order_id):
    try:
        project_order = ProjectOrder.objects.get(pk=project_order_id)
    except ProjectOrder.DoesNotExist:
        return False
    total_score = 0
    fields = []
    graphic_result = ProjectOrderGraphicFile.objects.\
        filter(project_order_id=project_order_id).count()
    if graphic_result > 0:
        total_score += SCORE_POINT * 2
    else:
        fields.append({'stage': 5, 'field': 'file'})
    project_order.stage_5_fields = fields
    project_order.stage_5_percent = total_score
    project_order.save()
    project_order.percent = calculate_project_order_percent(project_order)
    project_order.save()


def calculate_project_order_percent(project_order):
    return project_order.stage_1_percent + project_order.stage_2_percent + project_order.stage_3_percent \
           + project_order.stage_4_percent + project_order.stage_5_percent


def check_all_stages(project_order_id):
    check_first_stage(project_order_id)
    check_second_stage(project_order_id)
    check_third_stage(project_order_id)
    check_fourth_stage(project_order_id)
