USER_TYPE_CHOICES = (
    ('admin', 'Администратор'),
    ('user', 'Пользователь'),
    ('iicas', 'Члены МИЦАИ'),
    ('council', 'Член Совета'),
)

USER_LANGUAGES = (
    ('ru', 'Русский'),
    ('en', 'English'),
)
