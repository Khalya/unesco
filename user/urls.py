from django.urls import path

from user.views import RegistrationView, StepOneView, StepTwoView, StepThreeView, StepFourView, StepFiveView, \
    ajax_view_for_select, UserUpdateView, secondary_step_additionally_view, ProjectGraphicTwoUpdateView, StepSixView, \
    ProjectGraphicFourUpdateView, ProjectGraphicThreeUpdateView, ProjectGraphicFourDeleteView, \
    ProjectGraphicSixDeleteView, ChangePasswordView, UserLangChangeView, StepTwoActionsView, Activate, LoginView

urlpatterns = [
    path('accounts/login/', LoginView.as_view(), name='login'),
    path('accounts/registration/', RegistrationView.as_view(), name='register'),
    path('accounts/change_password/', ChangePasswordView.as_view(), name='change_password'),
    path('step/0/', StepOneView.as_view(), name='step_one'),
    path('step/1/', StepTwoView.as_view(), name='step_two'),
    path('step/1/actions/', StepTwoActionsView.as_view(), name='step_two_actions'),
    path('step/2/', StepThreeView.as_view(), name='step_three'),
    path('step/3/', StepFourView.as_view(), name='step_four'),
    path('step/4/', StepFiveView.as_view(), name='step_five'),
    path('step/5/', StepSixView.as_view(), name='step_six'),
    path('ajax/select/', ajax_view_for_select, name='ajax_select'),
    path('additionaly/delete/second/', secondary_step_additionally_view, name='ad_delete'),
    path('graphic/two/update/<int:pk>/', ProjectGraphicTwoUpdateView.as_view(), name='graphic_two_update'),
    path('accounts/update/<int:pk>', UserUpdateView.as_view(), name='user_update'),
    path('graphic/four/update/<int:pk>', ProjectGraphicFourUpdateView.as_view(), name='graphic_four_update'),
    path('graphic/four/delete/<int:pk>', ProjectGraphicFourDeleteView.as_view(), name='graphic_four_delete'),
    path('graphic/six/delete/<int:pk>', ProjectGraphicSixDeleteView.as_view(), name='graphic_six_delete'),
    path('graphic/three/update/<int:pk>', ProjectGraphicThreeUpdateView.as_view(), name='graphic_three_performers_update'),
    path('graphic/three/update/', ProjectGraphicThreeUpdateView.as_view(), name='graphic_three_performers_update_small'),
    path('user/update/lang/', UserLangChangeView.as_view(), name='update_user_lang'),
    path('activate/', Activate.as_view(), name='activate'),
]
