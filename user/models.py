from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.files.storage import FileSystemStorage
from django.db import models
from django_countries.fields import CountryField

from user.choices import USER_TYPE_CHOICES, USER_LANGUAGES
from django.utils.translation import ugettext_lazy as _


class MyUserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """

    def create_user(self, username, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.is_active = False
        user.user_type = 'user'
        user.save()
        return user

    def _create_user(self, username, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        user = self.model(username=username, **extra_fields)
        user.is_active = True
        user.set_password(password)
        user.user_type = 'user'
        user.save()
        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(username, password, **extra_fields)


class ScientistLevel(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Научные степени'
        verbose_name_plural = 'Научные степени'


class ScientistRank(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Научные звания'
        verbose_name_plural = 'Научные звания'


class Appeal(models.Model):
    name = models.CharField(verbose_name='Звание', max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Звание'
        verbose_name_plural = 'Звания'


class User(AbstractBaseUser, PermissionsMixin):
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Создан'))
    lang = models.CharField(verbose_name=_('Язык'), max_length=10, choices=USER_LANGUAGES, default='ru')
    first_name = models.CharField(verbose_name=_('Имя'), max_length=255)
    last_name = models.CharField(verbose_name=_('Фамилия'), max_length=255)
    middle_name = models.CharField(verbose_name=_('middle name'), max_length=255, null=True, blank=True)
    birthday = models.DateField(verbose_name=_('День рождения'), null=True)
    scientist_level = models.ForeignKey(ScientistLevel, verbose_name=_('Научная степень'), on_delete=models.CASCADE, null=True, blank=True)
    scientist_level_year = models.SmallIntegerField(verbose_name=_('Год'), default=0, null=True, blank=True)
    scientist_rank = models.ForeignKey(ScientistRank, verbose_name=_('Научное звание'), on_delete=models.CASCADE,
                                       null=True, blank=True)
    scientist_rank_year = models.SmallIntegerField(verbose_name=_('Год'), default=0, null=True, blank=True)
    work_place = models.TextField(verbose_name=_('Место работы'))
    work_place_short = models.CharField(verbose_name=_('Коротко о месте работы'), max_length=255, null=True, blank=True)
    work_position = models.CharField(verbose_name=_("Позиция на работе"), max_length=255, null=True, blank=True)
    public_count = models.IntegerField(verbose_name=_('Количество публикаций'), default=0, null=True, blank=True)
    address_zip = models.CharField(verbose_name=_('Zip адресс'), max_length=255)
    address_country = CountryField(verbose_name='Страна', blank=True, max_length=255)
    address_region = models.CharField(verbose_name=_('Регион'), max_length=255)
    address_street = models.CharField(verbose_name=_('Улица'), max_length=255)
    work_phone_number = models.CharField(verbose_name=_('Рабочий телефон'), max_length=255)
    home_phone_number = models.CharField(verbose_name=_('Домашний телефон'), max_length=255)
    username = models.CharField(unique=True, verbose_name=_('Адрес электронной почты'), max_length=255)
    appeal = models.ForeignKey(Appeal, verbose_name='Обращение к вам', on_delete=models.SET_NULL, null=True)
    passport_file = models.FileField(verbose_name=_('Паспорт'), null=True, blank=True)
    status = models.SmallIntegerField(verbose_name='Статус', default=0)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    user_type = models.CharField(_('Тип пользователя'),
                                 max_length=25,
                                 choices=USER_TYPE_CHOICES,
                                 default='user')
    date_joined = models.DateTimeField(auto_now_add=False,
                                       null=True,
                                       blank=True)
    photo = models.ImageField(storage=FileSystemStorage(location=settings.MEDIA_ROOT),
                              default='noimage.png', null=True, blank=True)
    USERNAME_FIELD = 'username'
    objects = MyUserManager()

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Пользователи'
        verbose_name_plural = 'Пользователи'
