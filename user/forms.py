from captcha.fields import ReCaptchaField
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django import forms
from django.forms import ModelForm
from modeltranslation.forms import TranslationModelForm

from project.models import ProjectOrderGraphicOne, ProjectOrderGraphicTwo
from user.models import User


class SignupForm(UserCreationForm):
    work_place_ru = forms.CharField(widget=forms.TextInput)
    work_place_en = forms.CharField(widget=forms.TextInput)
    captcha = ReCaptchaField(
        public_key='6LfvuygbAAAAAEl_9qZa735xJkkiAlT1A2ym_4R3',
        private_key='6LfvuygbAAAAAHJD_aCfO4C7Lu_RP9jvulxIB8W4'
    )

    class Meta:
        model = get_user_model()
        fields = ['photo', 'username', 'lang', 'first_name', 'last_name', 'middle_name', 'birthday', 'scientist_level', 'appeal',
                  'scientist_level_year', 'scientist_rank', 'scientist_rank_year', 'work_place_ru', 'work_place_en',
                  'work_place_short_ru', 'work_place_short_en', 'work_position_ru', 'work_position_en', 'public_count', 'address_zip', 'address_country',
                  'address_region', 'address_street', 'work_phone_number', 'home_phone_number',
                  'password1', 'password2', 'captcha']


class ChangePasswordForm(PasswordChangeForm):
    class Meta:
        model = get_user_model()
        fields = 'new_password1', 'new_password2', 'old_password'


class GraphicTwoForm(ModelForm):
    work_place_ru = forms.CharField(widget=forms.TextInput)
    work_place_en = forms.CharField(widget=forms.TextInput)

    class Meta:
        model = ProjectOrderGraphicTwo
        fields = '__all__'


class UpdateForm(forms.ModelForm):
    work_place_ru = forms.CharField(widget=forms.TextInput)
    work_place_en = forms.CharField(widget=forms.TextInput)

    class Meta:
        model = User
        fields = ['photo', 'username', 'lang', 'first_name', 'last_name', 'middle_name', 'birthday', 'scientist_level',
                  'scientist_level_year', 'scientist_rank', 'scientist_rank_year', 'work_place_ru', 'work_place_en',
                  'work_place_short_ru', 'work_place_short_en', 'work_position_ru', 'work_position_en', 'public_count', 'address_zip', 'address_country',
                  'address_region', 'address_street', 'work_phone_number', 'home_phone_number', 'appeal']
