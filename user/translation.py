from modeltranslation.translator import register, TranslationOptions
from user.models import User, ScientistLevel, ScientistRank, Appeal


@register(User)
class UserTranslationOptions(TranslationOptions):
    fields = ('work_place', 'work_place_short', 'work_position',)


@register(ScientistLevel)
class ScientistLevelTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(ScientistRank)
class ScientistRankTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Appeal)
class AppealTranslationOptions(TranslationOptions):
    fields = ('name',)
