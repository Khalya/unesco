from django.conf import settings
from django.contrib.auth import get_user_model, login, authenticate
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import PermissionDenied
from django.core.mail import EmailMessage, get_connection, EmailMultiAlternatives
from django.db.models import Sum
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import UpdateView, CreateView, DeleteView, FormView
from django.views.generic.base import View, TemplateView
from project.check_order_fill import check_first_stage, check_second_stage, check_third_stage, check_fourth_stage
from project.models import ProjectSphere, Type, ProjectOrder, ProjectOrderGraphicOne, ProjectOrderGraphicTwo, \
    ProjectOrderGraphicThree, ProjectOrderGraphicThreeResult, ProjectOrderGraphicFour, TypeEvent, TypeCategory, \
    ProjectOrderGraphicFile, ProjectOrderResearchArticle, ProjectOrderMonograph
from user.forms import SignupForm, GraphicTwoForm
from user.models import User
from django.utils.translation import activate

from user.tokens import account_activation_token


def send_verification_email(user_email, mail_subject, message):
    email = EmailMultiAlternatives(mail_subject, message, to=[user_email])
    email.attach_alternative(message, 'text/html')
    email.send()


class ChangePasswordView(FormView):
    template_name = 'registration/change_password.html'

    def get_form(self, form_class=None):
        return PasswordChangeForm(user=self.request.user, data=self.request.POST)

    def get_success_url(self):
        return '/'

    def form_valid(self, form):
        self.request.user.set_password(form.cleaned_data['new_password1'])
        self.request.user.save()
        return super(ChangePasswordView, self).form_valid(form)

    def form_invalid(self, form):
        print("FORM ERROR INVALID!")
        print(form)
        return super(ChangePasswordView, self).form_invalid(form)


class LoginView(FormView):
    template_name = 'registration/login.html'
    form_class = AuthenticationForm

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        if self.request.method == 'POST':
            print(self.request.POST)
            if 'username' in self.request.POST:
                try:
                    u = User.objects.get(username=self.request.POST['username'])
                    if not u.is_active:
                        context['user_not_active'] = True
                except:
                    pass
        return context

    def form_valid(self, form):
        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        if user is not None:
            login(self.request, user)
            return redirect(reverse('main-page'))
        return super(LoginView, self).form_valid(form)
    
    def form_invalid(self, form):

        print(form)
        return super(LoginView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('main-page')


class RegistrationView(FormView):
    template_name = 'registration/signup.html'
    form_class = SignupForm

    def get_success_url(self):
        return reverse('login') + "?from=register"

    def form_valid(self, form):
        form.instance.is_active = False
        form.save()
        activate(form.instance.lang)
        if form.instance.lang == 'en':
            mail_subject = 'Account activation!'
            uid = urlsafe_base64_encode(force_bytes(form.instance.id))
            token = account_activation_token.make_token(form.instance)
            activation_link = "{0}/activate/?uid={1}&token={2}&from=activate&lang={3}".format(get_current_site(self.request), uid, token, form.instance.lang)
            message = f'Please follow the link to activate your account:<br><a href="{activation_link}">{activation_link}</a> <br> If the link is not active, please copy and paste the link into the address bar of your browser.'
        else:
            mail_subject = 'Активация учетной записи!'
            uid = urlsafe_base64_encode(force_bytes(form.instance.id))
            token = account_activation_token.make_token(form.instance)
            activation_link = "{0}/activate/?uid={1}&token={2}&from=activate&lang={3}".format(get_current_site(self.request), uid, token, form.instance.lang)
            message = f'Пожалуйста, пройдите по ссылке для активации учетной записи:<br><a href="{activation_link}">{activation_link}</a> <br> Если ссылка не активна, просим скопировать ссылку и вставить в адресную строку браузера.'
        send_verification_email(form.instance.username, mail_subject, message)
        return super(RegistrationView, self).form_valid(form)

    def form_invalid(self, form):
        print("FORM_INVALID!")
        return super(RegistrationView, self).form_invalid(form)


class StepOneView(TemplateView):
    template_name = 'add_project_step_0.html'

    def dispatch(self, request, *args, **kwargs):
        # if request.user.id == 278:
        #     return super(StepOneView, self).dispatch(request, *args, **kwargs)
        try:
            type_event = TypeEvent.objects.get(id=request.GET.get('type_event', None))
        except TypeEvent.DoesNotExist:
            type_event = TypeEvent.objects.get(id=request.POST.get('type_event', None))
        if type_event.end_time < timezone.now():
            raise PermissionDenied
        else:
            return super(StepOneView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(StepOneView, self).get_context_data(**kwargs)
        context['spheres'] = ProjectSphere.objects.all()
        context['types'] = Type.objects.all()
        context['category_of_types'] = TypeCategory.objects.all()
        context['type_event'] = self.request.GET.get('type_event', None)
        project_order = self.request.GET.get('project_order', None)
        try:
            context['project_order'] = ProjectOrder.objects.get(id=project_order)
        except ProjectOrder.DoesNotExist:
            pass
        return context

    def post(self, request):
        try:
            title_ru = request.POST.get('title_ru', None)
            title_en = request.POST.get('title_en', None)
            spheres = request.POST.get('spheres', None)
            type = request.POST.get('type', None)
            type_event = request.POST.get('type_event', None)
            project_order = request.POST.get('project_order', None)
            type_category = request.POST.get('type_category', None)

            if project_order:
                order = ProjectOrder.objects.get(id=project_order)
                order.title_ru = title_ru
                order.title_en = title_en
                order.project_sphere = spheres
                order.type_id = type
                order.type_event_id = type_event
                order.type_category_id = type_category
                order.user_id = request.user.id
                order.save()
            else:
                order = ProjectOrder.objects.create(title_ru=title_ru, title_en=title_en,
                                                    project_sphere_id=spheres, type_id=type,
                                                    type_event_id=type_event, type_category_id=type_category,
                                                    user_id=request.user.id)
                ProjectOrderGraphicOne.objects.get_or_create(project_order_id=order.id)
                ProjectOrderGraphicThree.objects.get_or_create(project_order_id=order.id)
            return redirect(f'/{request.user.lang}/step/1/?project_order={order.id}')
        except Exception as e:
            print(e)
            return redirect(f'/{request.user.lang}/step/0/?type_event={type_event}')


class StepTwoView(TemplateView):
    template_name = 'add_project_step_1.html'

    def dispatch(self, request, *args, **kwargs):
        project_order = ProjectOrder.objects.get(pk=request.GET.get('project_order', None))
        if project_order.user == request.user:
            return super(StepTwoView, self).dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied

    def get_context_data(self, **kwargs):
        context = super(StepTwoView, self).get_context_data(**kwargs)
        context['project_order'] = ProjectOrder.objects.get(id=self.request.GET.get('project_order', None))
        context['form'] = GraphicTwoForm()
        context['research_articles'] = ProjectOrderResearchArticle.objects \
            .filter(graphic_one_id=self.request.GET.get('project_order', None))
        context['monographs'] = ProjectOrderMonograph.objects \
            .filter(graphic_one_id=self.request.GET.get('project_order', None))
        context['graphic_two'] = ProjectOrderGraphicTwo.objects.filter(
            project_order_id=self.request.GET.get('project_order', None))
        check_first_stage(self.request.GET.get('project_order'))
        graphic, created = ProjectOrderGraphicOne \
            .objects \
            .get_or_create(project_order_id=self.request.GET.get('project_order', None),
                           defaults={
                               "content_ru": '',
                               "content_en": '',
                               "info_ru": '',
                               "info_en": '',
                               "start_time": None,
                               "end_time": None
                           })
        context['graphic'] = graphic
        return context

    def post(self, request):
        project_order = request.GET.get('project_order', None)
        ######################################################
        content_ru = request.POST.get('content_ru', None)
        content_en = request.POST.get('content_en', None)
        start_time = request.POST.get('start_time', None)
        end_time = request.POST.get('end_time', None)
        info_ru = request.POST.get('info_ru', None)
        info_en = request.POST.get('info_en', None)
        work_readiness_a = request.POST.get('work_readiness_a', False)
        work_readiness_b = request.POST.get('work_readiness_b', False)
        work_readiness_c = request.POST.get('work_readiness_c', False)
        work_quality = request.POST.get('work_quality', False)
        novelty_a = request.POST.get('novelty_a', False)
        novelty_b = request.POST.get('novelty_b', False)
        novelty_c_ru = request.POST.get('novelty_c_ru', "")
        novelty_c_en = request.POST.get('novelty_c_en', "")
        novelty_d = request.POST.get('novelty_d', False)
        novelty_e = request.POST.get('novelty_e', False)
        novelty_f = request.POST.get('novelty_f', False)
        novelty_g = request.POST.get('novelty_g', False)
        have_a_good_translate = request.POST.get('have_a_good_translate', False)
        additionaly_amount = request.POST.get('additionaly_amount', False)
        relevance_ru = request.POST.get('relevance_ru', None)
        relevance_en = request.POST.get('relevance_en', None)
        interregional_nature_en = request.POST.get('interregional_nature_en', None)
        interregional_nature_ru = request.POST.get('interregional_nature_ru', None)
        last_submit = request.POST.get('last_submit', None)
        work_quality_file_ru = request.FILES.get('work_quality_file_ru', None)
        work_quality_file_en = request.FILES.get('work_quality_file_en', None)
        have_a_good_translate_file_ru = request.FILES.get('have_a_good_translate_file_ru', None)
        have_a_good_translate_file_en = request.FILES.get('have_a_good_translate_file_en', None)
        novelty_b_file_ru = request.FILES.get('novelty_b_file_ru', None)
        novelty_b_file_en = request.FILES.get('novelty_b_file_en', None)
        novelty_d_file_ru = request.FILES.get('novelty_d_file_ru', None)
        novelty_d_file_en = request.FILES.get('novelty_d_file_en', None)
        additionaly_amount_file_ru = request.FILES.get('additionaly_amount_file_ru', None)
        additionaly_amount_file_en = request.FILES.get('additionaly_amount_file_en', None)

        if work_readiness_a == 'on':
            work_readiness_a = True
        if work_quality == 'on':
            work_quality = True
        if work_readiness_b == 'on':
            work_readiness_b = True
        if work_readiness_c == 'on':
            work_readiness_c = True
        if novelty_a == 'on':
            novelty_a = True
        if novelty_b == 'on':
            novelty_b = True
        if novelty_d == 'on':
            novelty_d = True
        if novelty_e == 'on':
            novelty_e = True
        if novelty_f == 'on':
            novelty_f = True
        if novelty_g == 'on':
            novelty_g = True
        if have_a_good_translate == 'on':
            have_a_good_translate = True
        if additionaly_amount == 'on':
            additionaly_amount = True

        if last_submit:
            graphic, created = ProjectOrderGraphicOne.objects \
                .get_or_create(project_order_id=project_order,
                               defaults={
                                   "content_ru": content_ru,
                                   "content_en": content_en,
                                   "info_ru": info_ru,
                                   "info_en": info_en,
                                   'work_readiness_a': work_readiness_a,
                                   'work_readiness_b': work_readiness_b,
                                   'work_readiness_c': work_readiness_c,
                                   'novelty_a': novelty_a,
                                   'novelty_b': novelty_b,
                                   'novelty_d': novelty_d,
                                   'novelty_e': novelty_e,
                                   'novelty_f': novelty_f,
                                   'novelty_g': novelty_g,
                                   'have_a_good_translate': have_a_good_translate,
                                   'additionaly_amount': additionaly_amount,
                                   'work_quality_file_ru': work_quality_file_ru,
                                   'work_quality_file_en': work_quality_file_en,
                                   'have_a_good_translate_file_ru': have_a_good_translate_file_ru,
                                   'have_a_good_translate_file_en': have_a_good_translate_file_en,
                                   'novelty_b_file_ru': novelty_b_file_ru,
                                   'novelty_b_file_en': novelty_b_file_en,
                                   'novelty_c_ru': novelty_c_ru,
                                   'novelty_c_en': novelty_c_en,
                                   'novelty_d_file_ru': novelty_d_file_ru,
                                   'novelty_d_file_en': novelty_d_file_en,
                                   'additionaly_amount_file_ru': additionaly_amount_file_ru,
                                   'additionaly_amount_file_en': additionaly_amount_file_en,
                                   'relevance_ru': relevance_ru,
                                   'relevance_en': relevance_en,
                                   'interregional_nature_en': interregional_nature_en,
                                   'interregional_nature_ru': interregional_nature_ru,
                                   "start_time": start_time + '-01',
                                   "end_time": end_time + '-01',
                               })
            if not created:
                graphic.content_ru = content_ru
                graphic.content_en = content_en
                graphic.content_en = content_en
                graphic.work_readiness_a = work_readiness_a
                graphic.work_readiness_b = work_readiness_b
                graphic.work_readiness_c = work_readiness_c
                graphic.work_quality = work_quality
                graphic.novelty_a = novelty_a
                graphic.novelty_b = novelty_b
                graphic.novelty_c_ru = novelty_c_ru
                graphic.novelty_c_en = novelty_c_en
                graphic.novelty_d = novelty_d
                graphic.novelty_e = novelty_e
                graphic.novelty_f = novelty_f
                graphic.novelty_g = novelty_g
                graphic.have_a_good_translate = have_a_good_translate

                if novelty_b_file_ru:
                    graphic.novelty_b_file_ru = novelty_b_file_ru
                if novelty_b_file_en:
                    graphic.novelty_b_file_en = novelty_b_file_en

                if novelty_d_file_ru:
                    graphic.novelty_d_file_ru = novelty_d_file_ru
                if novelty_d_file_en:
                    graphic.novelty_d_file_en = novelty_d_file_en

                if additionaly_amount_file_ru:
                    graphic.additionaly_amount_file_ru = additionaly_amount_file_ru
                if additionaly_amount_file_en:
                    graphic.additionaly_amount_file_en = additionaly_amount_file_en
                graphic.additionaly_amount = additionaly_amount
                graphic.relevance_ru = relevance_ru
                graphic.relevance_en = relevance_en
                graphic.interregional_nature_en = interregional_nature_en
                graphic.interregional_nature_ru = interregional_nature_ru
                graphic.info_ru = info_ru
                graphic.info_en = info_en
                if have_a_good_translate_file_ru:
                    graphic.have_a_good_translate_file_ru = have_a_good_translate_file_ru
                if have_a_good_translate_file_en:
                    graphic.have_a_good_translate_file_en = have_a_good_translate_file_en
                if work_quality_file_ru:
                    graphic.work_quality_file_ru = work_quality_file_ru
                if work_quality_file_en:
                    graphic.work_quality_file_en = work_quality_file_en
                graphic.start_time = start_time + '-01'
                graphic.end_time = end_time + '-01'
                graphic.save()
            check_first_stage(project_order)
            return redirect(f'/{request.user.lang}/step/3/?project_order={project_order}')
        else:
            form = GraphicTwoForm(request.POST)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.work_place = form.instance.work_place_ru
                instance.project_order_id = project_order
                instance.work_place_short = form.instance.work_place_short_ru
                instance.work_position = form.instance.work_position_ru
                instance.save()
                dict_result = dict(instance.__dict__)
                dict_result['_state'] = ''
                activate('ru')
                dict_result['address_country_name_ru'] = instance.address_country.name
                activate('en')
                dict_result['address_country_name_en'] = instance.address_country.name
                dict_result['project_position_ru'] = instance.get_project_position_display()
                dict_result['appeal_name_ru'] = instance.appeal.name_ru
                dict_result['appeal_name_en'] = instance.appeal.name_en
                check_first_stage(self.request.GET.get('project_order', None))
                if request.POST.get('response_type') == 'json':
                    return JsonResponse(dict(dict_result), safe=True)
                else:
                    return render(request, self.template_name, self.get_context_data())
            else:
                self.get_context_data()['form_errors'] = form.errors
                return render(request, self.template_name, self.get_context_data())


class StepTwoActionsView(View):
    def post(self, request):
        print(request.POST)
        action = request.POST.get('action', None)
        project_order = request.POST.get('project_order')
        if action == 'research_article_create':
            plan_ru = request.POST.get('plan_ru', '')
            plan_en = request.POST.get('plan_en', '')
            cost = request.POST.get('cost', 0)
            funding_ru = request.POST.get('funding_ru', '')
            funding_en = request.POST.get('funding_en', '')
            research_article = ProjectOrderResearchArticle.objects.create(
                plan_ru=plan_ru,
                plan_en=plan_en,
                cost=cost,
                funding_ru=funding_ru,
                funding_en=funding_en,
                graphic_one_id=project_order
            )
            return JsonResponse({'plan_ru': plan_ru, 'plan_en': plan_en, 'cost': cost,
                                 'funding_ru': funding_ru, 'funding_en': funding_en,
                                 'research_article_id': research_article.id}, safe=True)
        elif action == 'research_article_update':
            research_article_id = self.request.POST.get('research_article_id', None)
            plan_ru = self.request.POST.get('plan_ru', '')
            plan_en = self.request.POST.get('plan_en', '')
            cost = self.request.POST.get('cost', 0)
            funding_ru = self.request.POST.get('funding_ru', '')
            funding_en = self.request.POST.get('funding_en', '')
            ProjectOrderResearchArticle.objects.filter(pk=research_article_id).update(
                plan_ru=plan_ru,
                plan_en=plan_en,
                cost_ru=cost,
                funding_ru=funding_ru,
                funding_en=funding_en
            )
            return JsonResponse({'plan_ru': plan_ru, 'plan_en': plan_en,
                                 'cost': cost,
                                 'funding_ru': funding_ru, 'funding_en': funding_en,
                                 'research_article_id': research_article_id}, safe=True)
        elif action == 'monograph_create':
            plan_ru = self.request.POST.get('plan_ru', '')
            plan_en = self.request.POST.get('plan_en', '')
            cost = self.request.POST.get('cost', 0)
            funding_ru = self.request.POST.get('funding_ru', '')
            funding_en = self.request.POST.get('funding_en', '')
            monograph = ProjectOrderMonograph.objects.create(
                plan_ru=plan_ru,
                plan_en=plan_en,
                cost=cost,
                funding_ru=funding_ru,
                funding_en=funding_en,
                graphic_one_id=project_order
            )
            return JsonResponse({'plan_ru': plan_ru, 'plan_en': plan_en, 'cost': cost,
                                 'funding_ru': funding_ru, 'funding_en': funding_en, 'monograph_id': monograph.id},
                                safe=True)
        elif action == 'monograph_update':
            monograph_id = self.request.POST.get('monograph_id', None)
            plan_ru = self.request.POST.get('plan_ru', '')
            plan_en = self.request.POST.get('plan_en', '')
            cost = self.request.POST.get('cost', 0)
            funding_ru = self.request.POST.get('funding_ru', '')
            funding_en = self.request.POST.get('funding_en', '')
            ProjectOrderResearchArticle.objects.filter(pk=monograph_id).update(
                plan_ru=plan_ru,
                plan_en=plan_en,
                cost_ru=cost,
                funding_ru=funding_ru,
                funding_en=funding_en
            )
            return JsonResponse({'plan_ru': plan_ru, 'plan_en': plan_en,
                                 'cost': cost,
                                 'funding_ru': funding_ru, 'funding_en': funding_en,
                                 'monograph_id': monograph_id}, safe=True)
        elif action == 'monograph_delete':
            ProjectOrderMonograph.objects.get(pk=request.POST.get('monograph_id')).delete()
            return JsonResponse({'yes': 'yes'}, safe=True)
        elif action == 'research_article_delete':
            ProjectOrderResearchArticle.objects.get(pk=request.POST.get('research_article_id'))
            return JsonResponse({'yes': 'yes'}, safe=True)
        elif action == 'delete_performer':
            ProjectOrderGraphicTwo.objects.filter(pk=request.POST.get('performer_id')).delete()
            return JsonResponse({'yes': 'yes'}, safe=True)
        else:
            return JsonResponse({'NO': 'NO'})


class StepThreeView(TemplateView):
    template_name = 'add_project_step_2.html'

    def dispatch(self, request, *args, **kwargs):
        project_order = ProjectOrder.objects.get(pk=request.GET.get('project_order', None))
        if project_order.user == request.user:
            return super(StepThreeView, self).dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied

    def get_context_data(self, **kwargs):
        context = super(StepThreeView, self).get_context_data(**kwargs)
        context['project_order'] = ProjectOrder.objects.get(id=self.request.GET.get('project_order', None))
        context['form'] = GraphicTwoForm()
        context['graphic_two'] = ProjectOrderGraphicTwo.objects.filter(
            project_order_id=self.request.GET.get('project_order', None))
        context['project_order_id'] = self.request.GET.get('project_order', None)
        check_second_stage(self.request.GET.get('project_order', None))
        return context

    def post(self, request):
        project_order = request.POST.get('project_order', None)
        print(project_order)
        form = GraphicTwoForm(request.POST)
        last_save = request.POST.get('last_save', None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.work_place = form.instance.work_place_ru
            instance.work_place_short = form.instance.work_place_short_ru
            instance.work_position = form.instance.work_position_ru
            instance.save()
            check_second_stage(self.request.GET.get('project_order', None))
            return render(request, self.template_name, self.get_context_data())

        graphic_two = ProjectOrderGraphicTwo.objects.filter(
            project_order_id=self.request.GET.get('project_order', None))
        if last_save == 'yes' and graphic_two:
            return redirect(f"/{request.user.lang}/step/3/?project_order={project_order}")
        else:
            # self.get_context_data()['warning_messages'] = _('Пожалуйста добаьте данные')
            # print(self.get_context_data())
            context = self.get_context_data()
            context['warning_message'] = True
            # print(context)
            return render(request, self.template_name, context)


class StepFourView(TemplateView):
    template_name = 'add_project_step_3.html'

    def dispatch(self, request, *args, **kwargs):
        project_order = ProjectOrder.objects.get(pk=request.GET.get('project_order', None))
        if project_order.user == request.user:
            return super(StepFourView, self).dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied

    def get_context_data(self, **kwargs):
        context = super(StepFourView, self).get_context_data(**kwargs)
        project_order = ProjectOrder.objects.get(id=self.request.GET.get('project_order', None))
        context['project_order'] = project_order
        graphic_three, created = ProjectOrderGraphicThree.objects.get_or_create(project_order_id=project_order.id)
        context['graphic_three'] = graphic_three
        ProjectOrderGraphicThreeResult.objects.filter(
            project_order_graphic_three_id=graphic_three.id).values_list('id', flat=True)
        context['graphic_three_results'] = ProjectOrderGraphicThreeResult.objects.filter(
            project_order_graphic_three_id=graphic_three.id)

        check_second_stage(self.request.GET.get('project_order', None))
        return context

    def post(self, request):
        response_type = request.POST.get('response_type', None)
        project_order = request.GET.get('project_order')
        project_step_three = request.GET.get('project_step_three')
        action_data = request.POST.get('action', None)
        attr1 = request.POST.get('attr1', None)
        attr2 = request.POST.get('attr2', None)
        attr3 = request.POST.get('attr3', None)
        attr1_en = request.POST.get('attr1_en', None)
        attr2_en = request.POST.get('attr2_en', None)
        attr3_en = request.POST.get('attr3_en', None)
        main_step_ru = request.POST.get('main_step_ru', None)
        main_step_en = request.POST.get('main_step_en', None)
        step_number = request.POST.get('step_number', None)
        graphic_three_id = request.POST.get('graphic_three_id', None)
        result_count = ProjectOrderGraphicThreeResult.objects.filter(
            project_order_graphic_three_id=graphic_three_id)
        if action_data == 'create_result':
            if attr1 and attr2 and attr3 and attr1_en and attr2_en and attr3_en:
                three_graphic = ProjectOrderGraphicThreeResult.objects.create(
                    project_order_graphic_three_id=graphic_three_id,
                    main_step_ru=main_step_ru, main_step_en=main_step_en, attr_one_ru=attr1,
                    attr_two_ru=attr2, attr_three_ru=attr3, attr_one_en=attr1_en,
                    attr_two_en=attr2_en, attr_three_en=attr3_en)
                three_graphic_dict = three_graphic.__dict__
                three_graphic_dict.__delitem__('_state')
                if response_type == 'json':
                    check_second_stage(project_order)
                    return JsonResponse({
                        'graphics_result': three_graphic_dict,
                        'graphic_three_id': graphic_three_id,
                        'elem_count': result_count.count()
                    }, safe=False)
        check_second_stage(project_order)
        return redirect(f'{request.user.lang}/step/4/?project_order={project_order}')


class StepFiveView(TemplateView):
    template_name = 'add_project_step_4.html'

    def dispatch(self, request, *args, **kwargs):
        project_order = ProjectOrder.objects.get(pk=request.GET.get('project_order', None))
        if project_order.user == request.user:
            return super(StepFiveView, self).dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied

    def get_context_data(self, **kwargs):
        context = super(StepFiveView, self).get_context_data(**kwargs)
        project_order = ProjectOrder.objects.select_related('type_category').get(id=self.request.GET.get('project_order', None))
        context['project_order'] = project_order
        graphic = ProjectOrderGraphicFour.objects.filter(
            project_order_id=self.request.GET.get('project_order', None))
        context['graphic_five'] = graphic
        amount = graphic.aggregate(Sum('amount'))['amount__sum']
        if not amount:
            amount = 0
        iicas_percent = round(amount / 100 * project_order.type.percent)
        total = amount + iicas_percent
        amount_category = project_order.type.amount
        context['amount'] = amount
        if amount_category < total:
            context['warning_message'] = True
        context['total'] = total
        context['iicas_percent'] = iicas_percent
        check_third_stage(project_order.id)
        return context

    def post(self, request):
        try:
            project_order = request.POST.get('project_order', None)
            name_ru = request.POST.get('name_ru', '')
            name_en = request.POST.get('name_en', '')
            unit_type_ru = request.POST.get('unit_type_ru', '')
            unit_type_en = request.POST.get('unit_type_en', '')
            price = request.POST.get('price', 0)
            amount = request.POST.get('amount', 0)
            count = request.POST.get('count', 0)
            extra_amount = request.POST.get('extra_amount', 0)
            extra_text_ru = request.POST.get('extra_text_ru', '-')
            extra_text_en = request.POST.get('extra_text_en', '-')
            if not extra_amount:
                extra_amount = 0
            if project_order and name_ru and name_en and price:
                proj = ProjectOrderGraphicFour.objects.create(project_order_id=project_order, name_ru=name_ru,
                                                              name_en=name_en, unit_type_ru=unit_type_ru,
                                                              unit_type_en=unit_type_en, count=count,
                                                              price=price, amount=amount, extra_amount=extra_amount,
                                                              extra_text_en=extra_text_en, extra_text_ru=extra_text_ru)
            check_third_stage(project_order)
            return render(request, self.template_name, self.get_context_data())
        except Exception as e:
            print(e)


class StepSixView(CreateView):
    model = ProjectOrderGraphicFile
    template_name = 'add_project_step_5.html'
    fields = ['file_ru', 'file_en']

    def get_success_url(self):
        try:
            project_order = self.request.POST.get('project_order', None)
        except Exception as e:
            print(e)
        return redirect(f'/{self.request.user.lang}/step/5/?project_order={project_order}')

    def form_valid(self, form):
        try:
            project_order = self.request.POST.get('project_order', None)
            form.instance.project_order_id = project_order
            form.save()
            check_fourth_stage(project_order)
        except Exception as e:
            print(e)
        return redirect(f'/{self.request.user.lang}/step/5/?project_order={project_order}')

    def get_context_data(self, **kwargs):
        context = super(StepSixView, self).get_context_data(**kwargs)
        project_order = self.request.GET.get('project_order', None)
        context['project_order'] = ProjectOrder.objects.get(pk=project_order)
        context['files'] = ProjectOrderGraphicFile.objects.filter(project_order_id=project_order)
        check_fourth_stage(project_order)
        return context


def ajax_view_for_select(request):
    type_id = request.POST['type_id'] if 'type_id' in request.POST else None
    type_category = TypeCategory.objects.filter(type_id=type_id)
    post_data = []
    for type in type_category.values():
        post_data.append(type)
    return JsonResponse({
        'data': post_data,
    })


def secondary_step_additionally_view(request):
    try:
        action = request.POST.get('action', None)
        if action == 'delete':
            graphic_id = request.POST.get('graphic_id', None)
            project_order = request.POST.get('project_order', None)
            ProjectOrderGraphicTwo.objects.get(id=graphic_id).delete()
            return redirect(f'/{request.user.lang}/step/1/?project_order={project_order}')
        elif action == 'project_order_graphic_three_result_delete':
            project_order_graphic_three_result = request.POST.get('project_order_graphic_three_result', None)
            project_order = request.POST.get('project_order')
            ProjectOrderGraphicThreeResult.objects.filter(id=project_order_graphic_three_result).delete()
            return redirect(f"/{request.user.lang}/step/3/?project_order={project_order}")
    except Exception as e:
        print(e)


class ProjectGraphicTwoUpdateView(UpdateView):
    model = ProjectOrderGraphicTwo
    template_name = 'step_three_update.html'
    fields = 'first_name', 'last_name', 'middle_name', 'scientist_level', 'scientist_level_year', 'scientist_rank', \
             'scientist_rank_year', 'work_place_ru', 'work_place_en', 'work_place_short_ru', 'work_place_short_en', 'work_position_ru', \
             'work_position_en', 'public_count', 'address_zip', 'address_country', 'address_region', 'address_street', 'work_phone_number', \
             'home_phone_number', 'email'

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.work_place = form.instance.work_place_ru
        instance.work_place_short = form.instance.work_place_short_ru
        instance.work_position = form.instance.work_position_ru
        instance.save()
        super(ProjectGraphicTwoUpdateView, self).form_valid(form)
        back_url = self.request.GET.get('back_url', None)
        project_order = self.request.GET.get('project_order', None)
        return redirect(str(back_url) + f"?project_order={project_order}")

    def get_success_url(self):
        back_url = self.request.GET.get('back_url', None)
        project_order = self.request.GET.get('project_order', None)
        return str(back_url) + f"?project_order={project_order}"

    def get_context_data(self, **kwargs):
        context = super(ProjectGraphicTwoUpdateView, self).get_context_data(**kwargs)
        return context


class UserUpdateView(UpdateView):
    model = User
    template_name = 'user_update.html'
    fields = '__all__'
    success_url = '/'

    def form_invalid(self, form):
        # username = self.request.POST.get('username', None)
        lang = self.request.POST.get('lang', None)
        first_name = self.request.POST.get('first_name', None)
        last_name = self.request.POST.get('last_name', None)
        middle_name = self.request.POST['middle_name'] if 'middle_name' in self.request.POST else ''
        scientist_level = self.request.POST.get('scientist_level', None)
        scientist_level_year = self.request.POST.get('scientist_level_year', None)
        scientist_rank = self.request.POST.get('scientist_rank', None)
        scientist_rank_year = self.request.POST.get('scientist_rank_year', None)
        work_place_ru = self.request.POST.get('work_place_ru', None)
        appeal = self.request.POST.get('appeal', None)
        work_place_short_ru = self.request.POST.get('work_place_short_ru', None)
        work_place_en = self.request.POST.get('work_place_en', None)
        work_place_short_en = self.request.POST.get('work_place_short_en', None)
        work_position_en = self.request.POST.get('work_position_en', None)
        public_count = self.request.POST.get('public_count', None)
        address_zip = self.request.POST.get('address_zip', None)
        address_country = self.request.POST.get('address_country', None)
        address_region = self.request.POST.get('address_region', None)
        address_street = self.request.POST.get('address_street', None)
        work_phone_number = self.request.POST.get('work_phone_number', None)
        home_phone_number = self.request.POST.get('home_phone_number', None)
        photo = self.request.FILES.get('photo', None)
        if photo:
            User.objects.filter(id=self.request.user.id).update(lang=lang, first_name=first_name,
                                                                last_name=last_name, middle_name=middle_name,
                                                                scientist_level_id=scientist_level,
                                                                scientist_level_year=scientist_level_year,
                                                                scientist_rank_id=scientist_rank,
                                                                scientist_rank_year=scientist_rank_year,
                                                                work_place_ru=work_place_ru,
                                                                work_place_short_ru=work_place_short_ru,
                                                                work_place_en=work_place_en,
                                                                work_place_short_en=work_place_short_en,
                                                                work_position_en=work_position_en,
                                                                public_count=public_count,
                                                                address_zip=address_zip,
                                                                address_country=address_country,
                                                                address_region=address_region,
                                                                address_street=address_street,
                                                                work_phone_number=work_phone_number,
                                                                home_phone_number=home_phone_number,
                                                                appeal_id=appeal,
                                                                )
            self.request.user.photo = photo
            self.request.user.save()
        else:
            user = User.objects.filter(id=self.request.user.id).update(lang=lang, first_name=first_name,
                                                                       last_name=last_name, middle_name=middle_name,
                                                                       scientist_level_id=scientist_level,
                                                                       scientist_level_year=scientist_level_year,
                                                                       scientist_rank_id=scientist_rank,
                                                                       scientist_rank_year=scientist_rank_year,
                                                                       work_place_ru=work_place_ru,
                                                                       work_place_short_ru=work_place_short_ru,
                                                                       work_place_en=work_place_en,
                                                                       work_place_short_en=work_place_short_en,
                                                                       work_position_en=work_position_en,
                                                                       public_count=public_count,
                                                                       address_zip=address_zip,
                                                                       address_country=address_country,
                                                                       address_region=address_region,
                                                                       address_street=address_street,
                                                                       work_phone_number=work_phone_number,
                                                                       home_phone_number=home_phone_number,
                                                                       appeal_id=appeal,
                                                                       )
        super(UserUpdateView, self).form_invalid(form)
        return redirect('/')


class ProjectGraphicFourUpdateView(UpdateView):
    model = ProjectOrderGraphicFour
    template_name = 'step_four_update.html'
    fields = 'count', 'name', 'unit_type', 'price', 'amount', 'extra_amount', 'extra_text'

    def get_success_url(self):
        return f"/step/4/?project_order={self.object.project_order.id}"


class ProjectGraphicFourDeleteView(DeleteView):
    model = ProjectOrderGraphicFour

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return f"/step/4/?project_order={self.object.project_order.id}"


class ProjectGraphicSixDeleteView(View):
    model = ProjectOrderGraphicFile

    def get(self, request, *args, **kwargs):
        lang = request.GET.get('lang', None)
        if lang:
            if lang == 'en':
                ProjectOrderGraphicFile.objects.filter(pk=kwargs['pk']).update(file_en=None)
            elif lang == 'ru':
                ProjectOrderGraphicFile.objects.filter(pk=kwargs['pk']).update(file_ru=None)
        try:
            file = ProjectOrderGraphicFile.objects.get(pk=kwargs['pk'])
            if not file.file_en and not file.file_ru:
                file.delete()
        except ProjectOrderGraphicFile.DoesNotExist:
            pass
        return redirect(f"/{self.request.user.lang}/step/5/?project_order={request.GET.get('project_order')}")


class ProjectGraphicThreeUpdateView(UpdateView):
    model = ProjectOrderGraphicThreeResult
    template_name = 'step_performens_update.html'
    fields = 'main_step_ru', 'main_step_en', 'attr_one_en', 'attr_one_ru', 'attr_two_en', \
             'attr_two_ru', 'attr_three_en', 'attr_three_ru',

    def get_success_url(self):
        return f"/step/3/?project_order={self.object.project_order_graphic_three.project_order.id}"


class UserLangChangeView(View):
    def post(self, request):
        change_ru = request.POST.get('change_ru', None)
        change_en = request.POST.get('change_en', None)
        back_url = request.POST.get('back_url', None)
        user = request.user

        if change_ru:
            user.lang = 'ru'
            user.save()
        elif change_en:
            user.lang = 'en'
            user.save()
        return redirect(back_url)


class Activate(View):
    def get(self, request):
        try:
            uidb64 = request.GET.get('uid', None)
            token = request.GET.get('token', None)
            lang = request.GET.get('lang', None)
            activate(lang)
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = get_user_model().objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, get_user_model().DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            # activate user and login:
            user.is_active = True
            user.save()
            login(request, user, backend='user.User')
            return redirect(reverse('login') + "?from=activate")

        else:
            return HttpResponse('Activation link is invalid!')
