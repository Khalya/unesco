from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from modeltranslation.admin import TranslationAdmin, TabbedTranslationAdmin

from user.models import User, ScientistLevel, ScientistRank, Appeal


@admin.register(User)
class CustomUserAdmin(UserAdmin, TabbedTranslationAdmin):
    list_display = 'username', 'first_name', 'user_type', 'is_superuser',
    filter_horizontal = 'user_permissions',
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'user_type', 'photo', 'lang')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


@admin.register(ScientistLevel)
class ScientistLevelAdmin(TabbedTranslationAdmin):
    pass


@admin.register(ScientistRank)
class ScientistRankAdmin(TabbedTranslationAdmin):
    pass


@admin.register(Appeal)
class AppealAdmin(TabbedTranslationAdmin):
    pass
