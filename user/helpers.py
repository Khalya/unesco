import math
from django.core.mail import EmailMessage

from project.check_order_fill import check_first_stage
from project.models import ProjectOrder, ProjectOrderGraphicOne


def project_order_percent_plus(order_id, fields_count):
    percent = fields_count * 5
    project_order = ProjectOrder.objects.get(pk=order_id)
    project_order.percent = project_order.percent + math.ceil(percent)
    project_order.save()
    return int(math.ceil(project_order.percent))


def send_verification_email(user_email, mail_subject, message):
    email = EmailMessage(mail_subject, message, to=[user_email])
    email.send()



